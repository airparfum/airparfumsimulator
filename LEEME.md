# Índice de contenido Apps Generador de APIs + Simulador de Máquina AirParfum 

En este directorio se puede encontrar tanto documentación como ficheros de instalación de Apps.


## Documentación disponible

1. AirParfum_APIs_Websocket_ES.pdf - Documento de definición de APIs soportadas por una Máquina AirParfum.

2. AirParfum_Simulator_Quick_Installation_Guide_ES.pdf - Guía de instalación rápida de las Apps Generador de APIs y   Simulador de Máquina AirParfum.


## Ejecutables disponibles

1. Generator-1.1.0-win-x86-Setup.exe - Fichero de instalación de la App Generador de APIs AirParfum para sistema Windows 32/64 bits.

2. Simulator-1.1.0-win-x86-Setup.exe - Fichero de instalación de la App Simulador de Máquina AirParfum para sistema Windows 32/64 bits.

# Apps APIs Generator + AirParfum Machine Simulator Directory Index

In this directory you can find both documents and installation files for Apps.


## Available documents

1. AirParfum_APIs_Websocket_ES.pdf - This document sets the definition for the AirParfum Machine supported APIs.

2. AirParfum_Simulator_Quick_Installation_Guide_ES.pdf - This is the quick installation guide for both the APIs Generator and the AirParfum Machine Simulator Apps.


## Installation files available

1. Generator-1.1.0-win-x86-Setup.exe - Windows 32/64 bit compatible installation file for the APIs Generator App.

2. Simulator-1.1.0-win-x86-Setup.exe - Windows 32/64 bit compatible installation file for the AirParfum Machine Simulator App.
